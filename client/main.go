package main

import (
    "bufio"
    "fmt"
    "github.com/streadway/amqp"
    "log"
    "math/rand"
    "os"
)

func main() {
    var username string
    fmt.Println("Your username:")
    fmt.Scanf("%s\n", &username)
    fmt.Println(username)

    conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
    if err != nil {
        log.Fatalf(err.Error())
    }
    defer conn.Close()

    ch, err := conn.Channel()
    if err != nil {
        log.Fatalf(err.Error())
    }
    defer ch.Close()

    sessionId := fmt.Sprint(rand.Intn(2))
    fmt.Printf("You is going to redirect one of two chat sessions randomly, your chat session is %s\n", sessionId)

    err = ch.ExchangeDeclare(
        sessionId,
        "fanout",
        true,
        false,
        false,
        false,
        nil,
    )
    if err != nil {
        log.Fatalf(err.Error())
    }

    q, err := ch.QueueDeclare(
        "",
        false,
        false,
        false,
        false,
        nil,
    )
    if err != nil {
        log.Fatalf(err.Error())
    }

    err = ch.QueueBind(
        q.Name,
        "",
        sessionId,
        false,
        nil,
    )
    if err != nil {
        log.Fatalf(err.Error())
    }

    messages, err := ch.Consume(
        q.Name,
        username,
        true,
        false,
        false,
        false,
        nil,
    )
    if err != nil {
        log.Fatalf(err.Error())
    }

    go func() {
        for d := range messages {
            log.Printf("%s", d.Body)
        }
    }()

    sc := bufio.NewScanner(os.Stdin)
    sc.Split(bufio.ScanLines)
    for {
        if sc.Scan() {
            err = ch.Publish(
                sessionId,
                "",
                false,
                false,
                amqp.Publishing{
                    ContentType: "text/plain",
                    Body:        []byte(fmt.Sprintf("%s: %s\n", username, sc.Text())),
                })
        }

    }
}
