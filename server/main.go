package main

import (
    "fmt"
    "github.com/streadway/amqp"
    "log"
)

func main() {
    conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
    if err != nil {
        log.Fatalf(err.Error())
    }
    defer conn.Close()

    ch, err := conn.Channel()
    if err != nil {
        log.Fatalf(err.Error())
    }
    defer ch.Close()
    err = ch.ExchangeDeclare(
        "chat",
        "fanout",
        true,
        false,
        false,
        false,
        nil,
    )
    if err != nil {
        log.Fatalf(err.Error())
    }

    fmt.Println("Ready")
    forever := make(chan bool)
    <-forever
}
